<?php
class Markshust_Devready_Block_GoogleAnalytics_Ga
    extends Mage_GoogleAnalytics_Block_Ga
{
    protected function _toHtml()
    {
        if (!Mage::getStoreConfig('google/analytics/devready_active_development')) {
            return '';
        }
        
        return parent::_toHtml();
    }
}
