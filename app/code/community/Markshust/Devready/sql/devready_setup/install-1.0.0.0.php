<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$rewrite = Mage::getModel('core/url_rewrite');
$storesIds = Mage::getModel('core/store')->getCollection()->getAllIds();

foreach ($storesIds as $storeId) {
    $rewrite->setStoreId($storeId)
        ->setIdPath('robots.txt')
        ->setRequestPath('robots.txt')
        ->setTargetPath('devready/robots/index')
        ->setIsSystem(true)
        ->save();
}

Mage::getModel('core/config')->saveConfig('web/seo/devready_robots_production', 'User-agent: *
Disallow: ');
Mage::getModel('core/config')->saveConfig('web/seo/devready_robots_development', 'User-agent: *
Disallow: /');

$installer->endSetup();
