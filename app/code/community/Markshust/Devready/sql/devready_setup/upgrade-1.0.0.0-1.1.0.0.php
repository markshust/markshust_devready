<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

Mage::getModel('core/config')->saveConfig('google/analytics/devready_active_development', 0);

$installer->endSetup();
