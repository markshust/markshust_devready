<?php
class Markshust_Devready_RobotsController
    extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout(false);
        
        if (Mage::getIsDeveloperMode()) {
            $data = Mage::getStoreConfig('web/seo/devready_robots_development');
        } else {
            $data = Mage::getStoreConfig('web/seo/devready_robots_production');
        }
        
        $this->getResponse()
            ->setBody($data)
            ->setHeader('Content-Type', 'text/plain');
        
        $this->renderLayout();
    }
}
