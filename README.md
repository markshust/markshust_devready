# Devready

Devready is a Magento module that creates a custom robots.txt file and disables Google Analytics for development/staging environments.

This module detects the `MAGE_IS_DEVELOPER_MODE` environment variable to determine if a site is a development site. It's recommended to set this to on for both dev and staging environments. You can set this either as an Apache environment variable, or in your index.php file.

## Robots.txt

If you forget to disable the `robots.txt` file on development or staging, your website could get indexed by search engines. Please ensure you do not have a robots.txt file in the root of your Magento instance; if you do, that file will take precedence.

#### Default production value
    User-agent: *
    Disallow: 

#### Default development value
    User-agent: *
    Disallow: /

You can edit these values in Admin > System > Configuration > Web > Search Engines Optimization.

## Google Analytics

If you forget to disable the Google Analytics tracking code on development or staging, your website analytics could get convoluted by tracking your whereabouts on these environments.

#### Default value
A new field has been created at Admin > System > Configuration > Sales > Google API > Google Analytics, named "Enable (Development)". Set this to No to disable for development environments. By default this is set to No.

## FREE

This is a free module provided by Mark Shust. There is no warranty of any kind, and you are free to do with it what you wish. Mark, however, would appreciate it if you [bought him a beer][1].

[1]: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=mark%40shust%2ecom&lc=US&item_name=Mark%20Shust%20Beer%20Fund&no_note=0&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHostedGuest
